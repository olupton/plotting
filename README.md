Collection of scripts to do with making nice matplotlib plots from ROOT objects.

Basic idea is to have:
- a script (e.g. `convert_root.py`) that is specific to your problem, which uses
  the utilities in `json_conversion.py` to write a serialised python dictionary
  (.json.bz2 preferred) containing the information about the plot
- a generic script (`plotjson.py`) that reads the serialised dictionary and
  renders the contents
  
Having the extra step (producting the serialised dictionaries) means that there
is a machine-readable file containing all the plot data, allowing future
adjustments without needing to have the original data or script/code that
produced it.

You can use an alias like:
`alias plotjson='lb-run --ext matplotlib LCG/88 python ~/plotting/plotjson.py'`
to get an environment with python, matplotlib etc.
