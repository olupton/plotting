import sys, os, bz2, json
from ROOT import TFile
from json_conversion import tojson

f = TFile.Open("test.root")
files = sys.argv[1:]
hname = "MagUp_Run1"
h = f.Get(hname)

json_hist = tojson(h)
json_hist['xlabel'] = r'$p~\gevcunit$'
json_hist['ylabel'] = r'$\eta$'
json_hist['curves']['data']['kwargs']['norm'][1]['vmin'] = 1e-2
json_hist['rcParams']  = { 'figure.figsize' : (12, 8) }
json_hist['curves']['line'] = {
    'command' : 'plot',
    'args' : [ [ 40.0, 40.0 ], json_hist['ylims'] ],
    'kwargs' : {
      'color' : 'red'
      }
    }
json_hist['text'] = [
    [ 0.69, 0.95, r'\lhcb' ]
    ]

js = { hname : json_hist }
with bz2.BZ2File('test.json.bz2', 'w') as ofile:
  json.dump(js, ofile)
