#!/usr/bin/env python
"""
Utilities for saving matplotlib commands to JSON files.
"""
from __future__ import print_function


def listsfromhist(hist, overunder=False, normpeak=False, **kwargs):
    from ROOT import TH1
    x, y, x_err, y_err = [], [], [], []
    if isinstance(hist, TH1):
        bin_min = 0 if overunder else 1
        bin_max = hist.GetNbinsX()
        if overunder:
            bin_max += 1
        for n in range(bin_min, bin_max+1):
            x.append(hist.GetXaxis().GetBinCenter(n))
            y.append(hist.GetBinContent(n))
            x_err.append(hist.GetXaxis().GetBinWidth(n)*0.5)
            y_err.append(hist.GetBinError(n))
    else:
        npts = hist.GetN()
        data = {}
        xscale = kwargs.get('xscale', 1.0)
        for v in ['X', 'Y', 'EXlow', 'EXhigh', 'EYlow', 'EYhigh', 'EX', 'EY']:
            try:
                arr = getattr(hist, 'Get'+v)()
                scale = xscale if 'X' in v else 1.0
                tmp = [arr[n]*scale for n in range(npts)]
                data[v] = tmp
            except IndexError:
                pass
        x, y = data['X'], data['Y']
        if 'EXlow' in data and 'EXhigh' in data:
            x_err = [data['EXlow'], data['EXhigh']]
        elif 'EX' in data:
            x_err = data['EX']

        if 'EYlow' in data and 'EYhigh' in data:
            y_err = [data['EYlow'], data['EYhigh']]
        elif 'EY' in data:
            y_err = data['EY']

    if normpeak:
        ymax = max(y)
        y = [yy/ymax for yy in y]
        try:
            y_err = [
                [down/ymax for down in y_err[0]],
                [up/ymax for up in y_err[1]]
            ]
        except:
            y_err = [err/ymax for err in y_err]

    return x, y, x_err, y_err


def curvefromhist(hist):
    # JSON curve dict from ROOT histogram.
    x, y, x_err, y_err = listsfromhist(hist, overunder=True)
    return {
        'command': 'plot',
        'args': [x, y],
        'kwargs': {
            'color': 'blue',
            'ls': '-',
            'lw': 2
        }
    }


def np_histogram(data, **kwargs):
    """Use np.histogram *now* to automatically bin data, and prepare the *binned* data to be saved to JSON """
    import numpy as np
    np_vals = np.array(data)
    hist_vals, bin_edges = np.histogram(np_vals, **kwargs)
    width = float(bin_edges[1] - bin_edges[0])
    cmd = {
        'command': 'bar',
        'args': [[float(x) for x in bin_edges[:-1]], [float(x) for x in hist_vals]],
        'kwargs': {
            'width': width
        }
    }
    return cmd, width


def np_histograms(data_list, **kwargs):
    import numpy as np
    combined_data = np.concatenate(data_list)
    if 'range' not in kwargs:
        kwargs['range'] = (np.min(combined_data), np.max(combined_data))
    if 'bins' not in kwargs:
        kwargs['bins'] = int(
            np.ceil(np.sqrt(len(combined_data)/len(data_list))))
    fake_cmd, width = np_histogram(combined_data, **kwargs)
    cmds = [np_histogram(data, **kwargs)[0] for data in data_list]
    return cmds, width


def pointsfromhist(hist, **kwargs):
    from ROOT import TH2

    # Data points with error bars from ROOT histogram.
    if isinstance(hist, TH2):
        # need to handle the 2D case...
        # assume an arbitrary z scale for now
        import math
        histmin = hist.GetBinContent(hist.GetMinimumBin())
        histmax = hist.GetBinContent(hist.GetMaximumBin())
        xlow, xmax = hist.GetXaxis().GetXmin(), hist.GetXaxis().GetXmax()
        ylow, ymax = hist.GetYaxis().GetXmin(), hist.GetYaxis().GetXmax()
        xydata = [
            [
                max(0, hist.GetBinContent(i, j))/histmax
                for i in range(1, hist.GetNbinsX()+1)]
            for j in range(1, hist.GetNbinsY()+1)
        ]
        import numpy as np
        xydata_numpy = np.array(xydata)
        zlow, zmax = xydata_numpy[xydata_numpy > 0.0].min(), 1.0
        if abs(round(math.log10(zlow))-math.log10(zlow)) < 0.1:
            zlow = 10**(round(math.log10(zlow)))
        return '2d', {
            'command': 'imshow',
            'args': [xydata],
            'kwargs': {
                'interpolation': 'nearest',
                'aspect': 'auto',
                'origin': 'lower',
                'extent': [xlow, xmax, ylow, ymax],
                'cmap': 'viridis',
                'norm': ('LogNorm', {
                    'vmin': zlow,
                    'vmax': zmax
                })
            }
        }
    else:
        x, y, x_err, y_err = listsfromhist(hist, **kwargs)
        return '1d', {
            'command': 'errorbar',
            'args': [x, y],
            'kwargs': {
                'ecolor': 'black',
                'color': 'black',
                'xerr': x_err if x_err is not None and len(x_err) else None,
                'yerr': y_err,
                'fmt': 'o',
                'markersize': 7
            }
        }


def tojson(data, model=None, components={}, logy=True, pull=None,
           **kwargs):
    datadim, datapoints = pointsfromhist(data, **kwargs)
    xscale = kwargs.get('xscale', 1.0)
    plot = {
        'xlabel': data.GetXaxis().GetTitle(),
        'ylabel': data.GetYaxis().GetTitle(),
        'curves': {
            'data': datapoints
        }
    }

    if datadim == '1d':
        data_y = datapoints['args'][1]
        ymin = min([x for x in data_y if x > 0]) if logy else min(data_y)
        ymax = max(data_y)
        xmin = datapoints['args'][0][0]  # corrected by the x error below
        xmax = datapoints['args'][0][-1]  # ...
        try:
            # We have +- xerrors
            xmin -= datapoints['kwargs']['xerr'][0][0]  # -ve error on 0th bin
            xmax += datapoints['kwargs']['xerr'][-1][1]  # +ve error on top bin
        except TypeError:
            # we have a single xerr per point
            xmin -= datapoints['kwargs']['xerr'][0]  # -ve error on 0th bin
            xmax += datapoints['kwargs']['xerr'][-1]  # +ve error on top bin

        if logy:
            plot['yscale'] = 'log'
            import math
            xx = 10**math.floor(math.log10(ymax))
            ymax = math.ceil(ymax/xx)*xx
            if ymin != 0.0:
                xx = 10**math.floor(math.log10(ymin))
                ymin = math.floor(ymin/xx)*xx
        else:
            ymax *= 1.1
    else:
        xmin, xmax, ymin, ymax = datapoints['kwargs']['extent']

    plot['xlims'] = [xmin, xmax]
    plot['ylims'] = [ymin, ymax]

    if pull is not None:
        x, y, x_err, y_err = listsfromhist(pull, overunder=True)
        pullx, pully = [], []
        for n in xrange(len(x)):
            pullx += [x[n] - x_err[n], x[n] + x_err[n]]
            pully += [y[n]] * 2

        plot['pull'] = {
            'command': 'fill_between',
            'args': [pullx, 0, pully],
            'kwargs': {
                'color': 'black',
                'facecolor': 'black',
                'interpolate': False,
            }
        }

    if model is not None:
        plot['curves']['model'] = curvefromhist(model)

    if len(components) > 1:
        for compname, comp in components.iteritems():
            print('Component', compname)
            plot['curves'][compname] = curvefromhist(comp)
    return plot


def genrcParams(fig_width=7.5, font_size=12):
    """
    Generate some rcParams settings based on fig_width [cm] and font_size [pt]
    """
    inch_in_cm = 2.52
    fig_width_inches = fig_width / inch_in_cm
    capsize = font_size/4
    linewidth = font_size/15  # edges etc.
    plot_linewidth = linewidth*2  # lines that are actually on the plots
    legend_fontsize = font_size
    annotation_fontsize = 0.75 * font_size
    rcParams = {
        'font.size': font_size,
        'legend.fontsize': annotation_fontsize,
        'figure.figsize': (fig_width_inches, fig_width_inches),
        'axes.titlesize': font_size,
        'axes.labelsize': font_size,
        'axes.labelpad': font_size/6,
        'axes.linewidth': linewidth,
        'lines.linewidth': plot_linewidth,
        'lines.markersize': font_size/3,
        'errorbar.capsize': capsize,
        'xtick.top': True,
        'xtick.bottom': True,
        'ytick.left': True,
        'ytick.right': True,
        'lines.markeredgewidth': plot_linewidth,
    }
    for x in ['x', 'y']:
        rcParams.update({
            x + 'tick.major.size': font_size/2,
            x + 'tick.minor.size': font_size/4,
            x + 'tick.major.width': linewidth,
            x + 'tick.minor.width': linewidth,
            x + 'tick.major.pad': font_size/3,
            x + 'tick.minor.pad': font_size/3,
            x + 'tick.labelsize': font_size*10/12,
        })
    return rcParams
