#!/usr/bin/env python
"""
Script for calling matplotlib-like commands saved in the supplied .json[.bz2] files.

Usage: python plotjson.py some_file.json.bz2 [opts]
"""
from __future__ import print_function
from matplotlib import gridspec
from matplotlib import pyplot as plt
import matplotlib
import sys
import os
import argparse
import numpy as np
try:
    from collections.abc import Mapping
except ImportError:
    from collections import Mapping

parser = argparse.ArgumentParser(
    description='Execute matplotlib commands based on a .json file.')
parser.add_argument('-o', '--outdir', type=str, default=None,
                    help='Where to write the plots.')
parser.add_argument('-p', '--plots', type=str, nargs='+', default=None,
                    help='Explicitly list the plots to be processed from the input file. Otherwise all plots are processed.')
parser.add_argument('-s', '--small', action='store_true',
                    help='Optimise plots for smaller print size.')
parser.add_argument(
    'file', nargs=1, help='Optionally-compressed .json file to plot.')
args = parser.parse_args()
if args.plots is not None:
    args.plots = set(args.plots)


def load_file(filename):
    if filename.endswith('.json.xz'):
        import lzma
        import json
        with lzma.open(filename, 'rt') as ifile:
            data = json.load(ifile)
            root = filename[:-8]
    elif filename.endswith('.json.bz2'):
        import bz2
        import json
        with bz2.BZ2File(filename, 'r') as ifile:
            data = json.load(ifile)
            root = filename[:-9]
    elif filename.endswith('.json'):
        import json
        with open(filename, 'r') as ifile:
            data = json.load(ifile)
            root = filename[:-5]
    elif filename.endswith('.pkl'):
        import pickle
        with open(filename, 'rb') as ifile:
            data = pickle.load(ifile)
            root = filename[:-4]
    else:
        raise Exception("Don't know how to open:" + filename)
    root = os.path.basename(root)
    return data, root


if len(args.file) > 1:
    raise Exception(
        "Currently we don't support plotting multiple .json in one go.")
pfile = args.file[0]
opts = []
if args.small:
    opts.append('small')
print(args)
print("opts = %s" % (opts))

scriptdir = os.path.dirname(os.path.abspath(__file__))
print("scriptdir = %s" % (scriptdir))
basedir = os.path.dirname(pfile)
if args.outdir is None:
    outdir = os.path.join(basedir, 'figs')
else:
    outdir = args.outdir
print("cwd = %s" % (os.getcwd()))
print("outdir = %s" % (outdir))

if not os.path.exists(outdir):
    os.mkdir(outdir)

dumps, pfileroot = load_file(pfile)
oprefix = os.path.join(outdir, pfileroot)
print("oprefix = %s" % (oprefix))

matplotlib.use('PDF')
rcfile = os.path.join(scriptdir, 'matplotlibrc_lhcb' +
                      ('_small' if args.small else ''))
print("rcfile =", rcfile)
matplotlib.rc_file(rcfile)
matplotlib.rcParams['text.latex.preamble'] = ','.join([
    r'\usepackage{amsmath}',
    r'\usepackage{ifthen}',
    r'\usepackage{xfrac}',
    r'\usepackage{xspace}',
    r'\usepackage{upgreek}',
    r'\newboolean{uprightparticles}',
    r'\setboolean{uprightparticles}{true}',
    r'\newboolean{inbibliography}',
    r'\setboolean{inbibliography}{false}',
    r'\input{%s}' % (os.path.join(scriptdir, 'lhcb-symbols-def'))
])

plt.ioff()


def execute(axes, curve):
    cmd = curve['command']
    args = curve.get('args', [])
    kwargs = curve.get('kwargs', {})
    if cmd == 'imshow' and 'norm' in kwargs and type(kwargs['norm']) == list:
        normtype, normkwargs = kwargs['norm']
        if normtype == 'LogNorm':
            from matplotlib.colors import LogNorm
            kwargs['norm'] = LogNorm(**normkwargs)
    for k in kwargs.keys():
        if type(kwargs[k]) == list:
            kwargs[k] = tuple(kwargs[k])

    # Inspired by https://stackoverflow.com/questions/37852462/filled-errorbars-in-matplotlib-rectangles/37852631
    if cmd == 'rectangle':  # like errorbar but draw boxes
        from matplotlib.patches import Rectangle
        from matplotlib.collections import PatchCollection
        error_boxes = []
        x_data, y_data = args
        x_errs, y_errs = kwargs.pop('xerr'), kwargs.pop(
            'yerr')  # don't support asymmetric errors for now

        # Set some default values for the boxes
        def default_helper(key, value):
            if key not in kwargs:
                kwargs[key] = value
        default_helper('edgecolor', 'None')

        # Loop over data points; create box from errors at each point
        for xc, yc, xe, ye in zip(x_data, y_data, x_errs, y_errs):
            error_boxes.append(Rectangle((xc - xe, yc - ye),
                                         2.0 * xe, 2.0 * ye, **kwargs))

        # Create patch collection with specified colour/alpha
        pc = PatchCollection(error_boxes, **kwargs)

        # Add collection to axes
        axes.add_collection(pc)

        # Add to legend
        axes.add_patch(error_boxes[0])

        # Increment the colour counter
        axes.plot([], [])
    elif cmd == 'ellipse':  # draw ellipses at the specified points
        from matplotlib.collections import EllipseCollection
        xvals, yvals, wids, heights, angles = args
        ec = EllipseCollection(wids, heights, angles, units='xy', offsets=np.column_stack((xvals, yvals)),
                               transOffset=axes.transData, **kwargs)
        axes.add_collection(ec)
    else:
        ret = getattr(axes, cmd)(*args, **kwargs)

    if cmd == 'imshow':
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        divider = make_axes_locatable(axes)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(ret, cax=cax)


def execute_dump(dump, axes, rcParams):
    #pull = dump.get('pull', None)
    # if pull is not None:
    #  grid = gridspec.GridSpec(2, 1, height_ratios=[6,1])
    #  pull_axes = plt.subplot(grid[1])
    #  pull_axes.get_xaxis().set_ticks([ ])
    #  pull_axes.get_yaxis().set_ticks([-5,0,5])
    #  pull_axes.get_yaxis().set_ticks(range(-5,6), minor = True)
    #  pull_axes.set_ylim(-5, 5)
    #  execute(pull_axes, pull)
    # else:
    #  grid = gridspec.GridSpec(1, 1)

    #axes = plt.subplot(grid[0])
    axes.minorticks_on()
    if args.small:
        axes.locator_params(nbins=5)

     # Draw the data
    for curve_key in sorted(dump['curves'].keys()):
        curve = dump['curves'][curve_key]
        execute(axes, curve)

    def handle_key(key, method, update_kwargs=None, use_right_axes=False):
        if key in dump:
            colours = None
            if use_right_axes:
                if handle_key.axes_right is None:
                    handle_key.axes_right = axes.twinx()
                local_axes = handle_key.axes_right
            else:
                local_axes = axes
            key_args = dump[key]
            if isinstance(key_args, Mapping):
                kwargs = key_args.get('kwargs', {})
                if update_kwargs is not None:
                    kwargs.update(update_kwargs)
                if 'ticklabels' in key:
                    if 'pad' in kwargs:
                        if 'xticklabels' in key:
                            which = 'x'
                        elif 'yticklabels' in key:
                            which = 'y'
                        local_axes.tick_params(which, pad=kwargs['pad'])
                        del kwargs['pad']
                    if 'color' in kwargs:
                        colours = kwargs['color']
                        del kwargs['color']
                args = key_args.get('args', [])
                ret = getattr(local_axes, method)(*args, **kwargs)
            else:
                if update_kwargs is not None:
                    kwargs = update_kwargs
                else:
                    kwargs = {}
                ret = getattr(local_axes, method)(key_args, **kwargs)

            if colours is not None:
                for x, colour in zip(ret, colours):
                    x.set_color(colour)
    handle_key.axes_right = None

    handle_key('title', 'set_title')
    handle_key('xlims', 'set_xlim')
    handle_key('ylims', 'set_ylim')
    handle_key('ylims_right', 'set_ylim', use_right_axes=True)
    handle_key('xscale', 'set_xscale')
    handle_key('yscale', 'set_yscale')
    handle_key('xticks', 'set_xticks')
    handle_key('xticks_minor', 'set_xticks', {'minor': True})
    handle_key('yticks', 'set_yticks')
    handle_key('yticks_right', 'set_yticks', use_right_axes=True)
    handle_key('yticks_minor', 'set_yticks', {'minor': True})
    handle_key('yticks_minor_right', 'set_yticks', {
               'minor': True}, use_right_axes=True)
    if 'yticklabels' in dump and 'yscale' in dump and dump['yscale'] == 'log':
        from matplotlib.ticker import FixedFormatter
        axes.yaxis.set_major_formatter(FixedFormatter(dump.pop('yticklabels')))
    handle_key('xticklabels', 'set_xticklabels')
    handle_key('xticklabels_minor', 'set_xticklabels', {'minor': True})
    handle_key('yticklabels', 'set_yticklabels')
    handle_key('yticklabels_right', 'set_yticklabels', use_right_axes=True)
    handle_key('yticklabels_minor', 'set_yticklabels', {'minor': True})
    handle_key('tick_params', 'tick_params')

    for text in dump.get('text', []):
        kwargs = {
            'fontsize': 28,
            'verticalalignment': 'center',
            'horizontalalignment': 'center',
        }
        if len(text) > 3:
            kwargs.update(text[3])
        plt.text(*text[:3], transform=axes.transAxes, **kwargs)

    if 'xlabel' in dump:
        axes.set_xlabel(dump['xlabel'], ha='right', x=1,
                        labelpad=rcParams.get('axes.labelpad', 10))
    if 'ylabel' in dump:
        axes.set_ylabel(dump['ylabel'], ha='right', y=1,
                        labelpad=rcParams.get('axes.labelpad', 10))

    if dump.get('autoscale_view', False):
        axes.autoscale_view()

    reglegend, reglegend_kwargs = dump.get('reglegend', False), {}
    if isinstance(reglegend, Mapping):
        # if we're given a dictionary, interpret it as 'True' + some kwargs
        reglegend_kwargs = reglegend
        reglegend = True
    if reglegend:
        kwargs = {
            'loc': 'best',
            'framealpha': 0.0,
            'frameon': False,
            'handler_map': {},
        }
        kwargs.update(reglegend_kwargs)
        xerr_size = kwargs.pop('xerr_size', None)
        title_align = kwargs.pop('title_align', None)
        if xerr_size is not None:
            from matplotlib.container import ErrorbarContainer
            from matplotlib.legend_handler import HandlerErrorbar
            kwargs['handler_map'][ErrorbarContainer] = HandlerErrorbar(
                xerr_size=xerr_size)
        legend = axes.legend(**kwargs)
        if title_align is not None:
            legend._legend_box.align = title_align


for dumpname, dump in dumps.items():
    if args.plots is not None and dumpname not in args.plots:
        print("Skipping", dumpname, "due to -p/--plots option")
        continue

    rcParams = dump.get('rcParams', {})
    # Implement these changes, keep track of the old values
    old_rcParams = {}
    for key, val in rcParams.items():
        old_rcParams[key] = matplotlib.rcParams[key]
        matplotlib.rcParams[key] = val
        #print("Updating", key, "from", old_rcParams[key], "to", val)

    # See if we're doing subplots -- if so, need to handle that at this top level
    if 'subplots_kwargs' in dump:
        # Yes, we're doing subplots.
        fig, axes = plt.subplots(**dump['subplots_kwargs'])
        assert 'subplots' in dump
        for subplot_key, subplot in dump['subplots'].items():
            # subplot_key is something like '0_0' and tells us which subplot to draw it in
            try:
                # See if we're in a trivial 1D case
                subplot_index = int(subplot_key)
            except:
                raise Exception(">1D subplots unsupported.")
            execute_dump(subplot, axes[subplot_index], matplotlib.rcParams)
    else:
        if 'subplot' in dump:
            def helper(key, default):
                if key not in dump['subplot']:
                    dump['subplot'][key] = default

            def copy(key):
                if key in dump:
                    helper(key, dump[key])
            copy('xlims')
            copy('xscale')
            helper('xticklabels', [])
            helper('xticklabels_minor', [])
            grid = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
            subplot_axes = plt.subplot(grid[1])
            execute_dump(dump['subplot'], subplot_axes, matplotlib.rcParams)
        else:
            grid = gridspec.GridSpec(1, 1)
        axes = plt.subplot(grid[0])
        execute_dump(dump, axes, matplotlib.rcParams)

    outformats = [
        ('pdf', {}),
        #('png', { 'dpi' : 300 }),
        #('eps', { 'pad_inches' : 0.15})
    ]

    for suffix, kwargs in outformats:
        plt.savefig(oprefix + ("_" + dumpname if len(dumpname) else '') +
                    (("_" + '_'.join(opts)) if len(opts) else '') + "." + suffix, **kwargs)

    if dump.get('seplegend', False):
        import pylab
        figlegend = pylab.figure()
        legend = pylab.figlegend(
            *axes.get_legend_handles_labels(), loc='center', labelspacing=0.6)
        figlegend.canvas.draw()
        for suffix, kwargs in outformats:
            figlegend.savefig(oprefix + "_legend." + suffix, bbox_inches=legend.get_window_extent(
            ).transformed(figlegend.dpi_scale_trans.inverted()), **kwargs)

    plt.close()

    # Revert rcParams changes
    for key, val in old_rcParams.items():
        #print("Reverting", key, "to", val, "from", matplotlib.rcParams[key])
        matplotlib.rcParams[key] = old_rcParams[key]
